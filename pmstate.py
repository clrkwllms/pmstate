#!/usr/bin/python -tt

import os
import sys
import subprocess
import os.path
import glob
import argparse

cpupath = "/sys/devices/system/cpu"
cmds = ('enable', 'disable', 'list', 'help')

def get_online_cpus():
    p = os.path.join(cpupath, "cpu[0-9]*")
    cpuset = set()
    for cp in glob.glob(p):
        path=os.path.join(cp, "online")
        online=False
        if os.path.exists(path):
            with open(path, "r") as f:
                if f.read().strip() == "1":
                    online=True
        else:
            # special case for boot cpus
            online=True
        cpuset.add(os.path.basename(cp)[3:])
    return cpuset


def expand_range(r):
    if '-' not in r:
        return r
    s,e = r.split('-')
    l = []
    for i in range(int(s),int(e)+1):
        l.append(str(i))
    return ",".join(l)

def expand_cpu_list(cpulist):
    cpuset = set()
    for e in cpulist.split(','):
        t = expand_range(e)
        for i in t.split(","):
            cpuset.add(i)
    return cpuset


def syswrite(path, val):
    with open(path, "w") as f:
        f.write(val)
        f.close()

def sysread(path):
    with open(path, "r") as f:
        return f.read()

def enable_pmstates(cpulist):
    for c in cpulist:
        p = os.path.join(cpupath, f'cpu{c}', "cpuidle")
        states = glob.glob(os.path.join(p, "state[0-9]*"))
        for s in states:
            syswrite(os.path.join(s,"disable"), "0")


def disable_pmstates(cpulist):
    for c in cpulist:
        p = os.path.join(cpupath, f'cpu{c}', "cpuidle")
        states = glob.glob(os.path.join(p, "state[0-9]*"))
        for s in states:
            syswrite(os.path.join(s,"disable"), "1")

def list_pmstates(cpulist):
    cpus = [ int(c) for c in list(cpulist)]
    cpus.sort()
    for c in cpus:
        path = os.path.join(cpupath, f'cpu{c}', "cpuidle")
        states = glob.glob(os.path.join(path, 'state*'))
        states.sort()
        statevals = []
        for s in states:
            state = os.path.basename(s)
            val = sysread(os.path.join(s, 'disable'))
            if int(val) == 0:
                val = 'enabled'
            else:
                val = 'disabled'
            statevals.append(f'{state}:{val}')
        print (f'cpu{c:4}: {statevals!r}')


if __name__ == "__main__":

    args = argparse.ArgumentParser(description="Disable or Enable power management states for cpus")
    args.add_argument('--cpulist', dest='cpulist', help='specify cpus on which to operate')
    args.add_argument('command', help='disable, enable or list command')
    opts = args.parse_args()
    if opts.cpulist:
        cpulist=expand_cpu_list(opts.cpulist)
    else:
        cpulist=get_online_cpus()
    if opts.command not in cmds:
        args.print_help()
        sys.exit(-1)

    if opts.command == "disable":
        disable_pmstates(cpulist)
    elif opts.command == "enable":
        enable_pmstates(cpulist)
    list_pmstates(cpulist)
