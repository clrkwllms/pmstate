## Summary
The pmstate script is used to enable/disable and list the pmstates for
each online cpu in the system

## Details
Linux has a power-state transition control mechanism that is exported
in the /sys filesystem hierarchy:

``` Shell
/sys/devices/system/cpu/cpu<n>/cpuidle/state<i>
```

For each cpu in the system the cpuidle mechanism exposes a number of
power states where an increasing number indicates more power
savings. In each 'state<i>' directory is a 'disable' field to indicate
if the state should be disabled.

The 'pmstate' script will disable or enable these power states based
on an input command (disable or enable) and optionally a CPU-list of
cpus to be affected:

``` Shell
$ sudo python pmstate.py --cpulist=5-7 disable
```

The above command disables power-state transitions on cpus 5, 6
and 7.

``` Shell
$ sudo python pmstate.py --cpulist=5-7 enable
```

This command enables power-state transitions for cpus 5, 6 and 7.

The pmstate script may also list the available state transitions for a
cpulist and their current settings:

``` Shell
$ sudo python pmstate.py --cpulist=0-7 list
cpu   0: ['state0:enabled', 'state1:enabled', 'state2:enabled', 'state3:enabled', 'state4:enabled']
cpu   1: ['state0:enabled', 'state1:enabled', 'state2:enabled', 'state3:enabled', 'state4:enabled']
cpu   2: ['state0:enabled', 'state1:enabled', 'state2:enabled', 'state3:enabled', 'state4:enabled']
cpu   3: ['state0:enabled', 'state1:enabled', 'state2:enabled', 'state3:enabled', 'state4:enabled']
cpu   4: ['state0:enabled', 'state1:enabled', 'state2:enabled', 'state3:enabled', 'state4:enabled']
cpu   5: ['state0:enabled', 'state1:enabled', 'state2:enabled', 'state3:enabled', 'state4:enabled']
cpu   6: ['state0:enabled', 'state1:enabled', 'state2:enabled', 'state3:enabled', 'state4:enabled']
cpu   7: ['state0:enabled', 'state1:enabled', 'state2:enabled', 'state3:enabled', 'state4:enabled']
```
